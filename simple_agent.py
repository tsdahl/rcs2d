import socket
import sys
import time
import sock
import sp_exceptions

class Agent:
    def __init__(self):

        self.__connected = False
        self.__sock = None
        self.__parsing = False

    def connect(self, host, port):
        port = int(port)
        if self.__connected:
            msg = "Cannot connect while already connected, disconnect first."
            raise sp_exceptions.AgentConnectionStateError(msg)

        self.__sock = sock.Socket(host, port)
        init_msg = ''
        if port == 6000:
            init_msg = "(init A (version 18))"
        elif port == 6001:
            init_msg = "(init (version 18))"

        self.__sock.send(init_msg)
        time.sleep(0.0001)
        self.__connected = True
        print('connected')

    def __message_loop(self):
        while self.__parsing:
            raw_msg = self.__sock.recv()
            print(raw_msg)
        return

    def send_cmd(self, msg):
        self.__sock.send(msg)
        return
    def recv_msg(self):
        print(self.__sock.recv())
        return



if __name__ == "__main__":

    a = Agent()
    port = sys.argv[1]
    a.connect("localhost", port)
    while True:
        msg = input('next message')
        a.recv_msg()
        a.send_cmd(msg)











