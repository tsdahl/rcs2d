import gymnasium as gym
from gymnasium import spaces
import numpy as np
import sock
import time
import threading
import handler
from world_model import WorldModel
import sp_exceptions
import subprocess
import random


class Agent:
    def __init__(self):
        self.__connected = False
        self.__sock = None
        self.wm = None # World model to store info of this one player
        self.msg_handler = None
        self.__parsing = False
        self.__msg_thread = None
        self.__send_commands = False
        self.teamname = None
        self.action_handler = None
        self.normalizer = None

    def connect(self, host, port, teamname=None):
        port = int(port)
        if self.__connected:
            msg = "Cannot connect while already connected, disconnect first."
            raise sp_exceptions.AgentConnectionStateError(msg)

        self.__sock = sock.Socket(host, port)
        self.wm = WorldModel(handler.ActionHandler(self.__sock), port)
        self.teamname = teamname
        self.msg_handler = handler.MessageHandler(self.wm, self.__sock)

        # set up our threaded message receiving system
        self.__parsing = True  # tell thread that we're currently running
        self.__msg_thread = threading.Thread(target=self.__message_loop,
                                             name="message_loop")
        self.__msg_thread.daemon = True  # dies when parent thread dies

        # start processing received messages. this will catch the initial server
        # response and all subsequent communication.
        self.__msg_thread.start()

        # send the init message and allow the message handler to handle further
        # responses.
        init_address = self.__sock.address
        if teamname is not None:
            # meaning the agent is a player instead of a coach, so we initialize the action handler
            init_msg = "(init {} (version 18))".format(teamname)
            self.action_handler = handler.ActionHandler(port)
        else:
            init_msg = "(init (version 18))"

        self.__sock.send(init_msg)

        # wait until the socket receives a response from the server and gets its
        # assigned port.
        # Don't send anything before this step
        while self.__sock.address == init_address:
            time.sleep(0.0001)

        self.__connected = True

        return

    def coach_send(self, msg):
        """
        Designed for coach to send the cmd
        """
        self.__sock.send(msg)

    def disconnect(self):
        # don't do anything if not connected
        if not self.__connected:
            return

        # tell the loops to terminate
        self.__parsing = False

        # tell the server that we're quitting
        self.__sock.send("(bye)")

        # tell our threads to join, but only wait breifly for them to do so.
        # don't join them if they haven't been started (this can happen if
        # disconnect is called very quickly after connect).
        if self.__msg_thread.is_alive():
            self.__msg_thread.join(0.01)

        Agent.__init__(self)

    def __message_loop(self):
        """
        Handles messages received from the server.

        This SHOULD NOT be called externally, since it's used as a threaded loop
        internally by this object.  Calling it externally is a BAD THING!
        """

        # loop until we're told to stop
        while self.__parsing:
            # receive message data from the server and pass it along to the
            # world model as-is.  the world model parses it and stores it within
            # itself for perusal at our leisure.
            raw_msg = self.__sock.recv()
            msg_type, sim_time = self.msg_handler.handle_message(raw_msg)
            self.sim_time = sim_time

            # we send commands all at once every cycle, ie. whenever a
            # 'sense_body' command is received
            if msg_type == handler.ActionHandler.CommandType.SENSE_BODY:
                self.__send_commands = True


class ParameterizedActionSpace(gym.spaces.Space):

    def __init__(self):
        super(ParameterizedActionSpace, self).__init__()
        self.action_selection_space = spaces.Discrete(2)
        self.kick_parameters_space = spaces.Box(low=np.array([-100.0, -90.0]), high=np.array([100.0, 90.0]), shape=(2,), dtype=float)
        self.dash_parameters_space = spaces.Box(low=-100.0, high=100.0, shape=(1,), dtype=float)
        # The default range for dir of kick and dash is -180 to 180. That is redundant when we can have negative power.

    def sample(self, mask=None):
        action_type = self.action_selection_space.sample()

        if action_type == 0:
            parameter = self.kick_parameters_space.sample()

            return np.concatenate((np.array([action_type]), parameter))
        else:

            parameter = self.dash_parameters_space.sample()

            return np.concatenate((np.array([action_type]), parameter))


class RcssserverEnv(gym.Env):

    """
    coach=on: we initialize a coach to observe the accurate postions of objects and change the game mode without a referee
    extra_half_time=0: when nr_normal_halfs is played and the score is tie, we don't need extra time
    game_logging/text_logging=false: stop auto save of the games
    nr_normal_halfs=1: we don't see a half as an episode, we use coach to start and end an episode
    half_time: total time
    """

    def __init__(self):
        super(RcssserverEnv, self).__init__()
        self.agent = None
        self.server = None
        self.coach = None
        # Define the dimension of state space
        n = 116  # length
        low = np.full((116,), 0) # lower bound for each entry
        high = np.full((116,), 5) # Higher bound for each entry
        self.observation_space = spaces.box.Box(low=low, high=high, shape=(n,), dtype=float)
        self.action_space = ParameterizedActionSpace()
        self.sim_time = 0

        # Gym API

        self.reward = 0
        self.done = False
        self.coach_kick_count = 0
        self.agent_kick_count = 0
        self.effective_kick = False
        self.time = 0

        self.p = 0.5
        self.reset_time = 0

        # self.half_time = 30000
        # self.nr_normal_halfs = 1
        cmd = ('./rcssserver server::coach=on server::extra_half_time=0 server::game_logging=false'
               'server::text_logging=false '
                'server::recv_step=1 server::send_step=15 server::send_vi_step=10 server::sense_body_step=10 server::simulator_step=10')
                # When there is no referee, there's no need to use these parameters
                # 'server::nr_normal_halfs={} '
                # 'server::half_time={}'
                # 'server::use_offside = false'
                # .format(self.nr_normal_halfs, self.half_time))

        self.server = subprocess.Popen([cmd]
                       , cwd='/home/liwen/Downloads/rcssserver-18.1.3/src', shell=True)
        #self.monitor = subprocess.Popen(['./rcssmonitor']
         #                              , cwd='/home/liwen/Downloads/rcssmonitor-18.0.0/src',
         #                              shell=True)
        time.sleep(1)
        self.agent = Agent()
        self.agent.connect("localhost", 6000, teamname='A')
        self.coach = Agent()
        self.coach.connect("localhost", 6001)
        self.coach.coach_send('(eye on)')

    def calculate_reward(self, action_chosen):
        """
        Only kick will get the reward and the reward is delayed. Only when another effective kick happens
        or the episode is ended, the reward of the previous kick will be passed to the trainer.
        Thus, we need to identify if a kick is effective. By my estimation when the ball_agent_distance is about 1.085,
        a kick is effective. But there is random noise on the kickable margin. So the current method is to wait for one
        cycle to get the kick_count. Only effective kicks will be recorded by sense_body message.
        TODO: find a method that doesn't need waiting. Coach receive 'k' in see_global, that may be useful.
        """

        self.reward = self.coach.wm.reward
        if action_chosen == 0:
            i = 0
            while self.coach_kick_count == self.coach.wm.coach_kick_count:
                time.sleep(0.005)
                i += 1
                if i == 2:
                    self.effective_kick = False
                    return 0

            last_see_global = self.coach.wm.sim_time
            while self.agent.wm.sense_body_sim_time < last_see_global:
                time.sleep(0.001)

            if self.agent_kick_count != self.agent.wm.action_counts['kick_count']:

                self.effective_kick = True
                self.coach.wm.reward = 0
                return 0
            else:
                return -1

        else:
            temp = self.coach.wm.dash_reward
            self.coach.wm.dash_reward = 0
            # return temp
            return 0


    def reset(self, *, options=None, seed=None, init=False):
        self.reset_time += 1
        if self.reset_time%100 == 0:
            self.p -= 0.001
        self.agent.wm.state = [None]
        self.reward = 0
        self.coach.wm.reward = 0
        self.coach.wm.done = False
        self.coach.wm.episode_start_time = self.coach.wm.sim_time
        if init:
            self.coach.coach_send('(start)')

        while self.agent.wm.state[0] is None:
            x = random.randint(25, 45)  # 25 45 x = random.randint(15, 51)
            y = random.randint(-20, 20)  # -20 20y = random.randint(-34, 34)
            dice = random.random()
            if dice >= self.p:
                eps = random.uniform(0.1, 8) # 8
                self.coach.coach_send('(move (player A 1) {} {})'.format(x - eps, y))

                self.coach.coach_send('(move (ball) {} {})'.format(x, y))
                time.sleep(0.01)
            else:
                self.coach.coach_send('(move (player A 1) {} {})'.format(x - 0.1, y))

                self.coach.coach_send('(move (ball) {} {})'.format(x, y))
                time.sleep(0.01)

        self.coach.wm.episode_start_time = self.coach.wm.sim_time  # time the start time of the episode and stop it
        # when max length of an episode is reached
        self.time = self.coach.wm.sim_time  # help agent act synchronously with sim time
        self.coach.wm.episode_start = True  # tell the coach to start observe and record rewards
        return self.agent.wm.state, {}

    def step(self, action):
        """
        Action = [action_chosen, kick_power, kick_dir, dash_power, dash_dir]

        """
        while self.coach.wm.sim_time == self.time:
            time.sleep(0.005)

        if action[0] == 0:
            self.coach_kick_count = self.coach.wm.coach_kick_count
            self.agent_kick_count = self.agent.wm.action_counts['kick_count']
            self.agent.coach_send('(kick {} {})'.format(action[1], action[2]))

        else:
            self.agent.coach_send('(dash {} {})'.format(action[1], 0))  # Currently we only use dash power

        self.time = self.coach.wm.sim_time

        return self.agent.wm.state, self.calculate_reward(action[0]), self.coach.wm.done, False, {}

    def close(self):
        self.server.terminate()
