
import torch
import torch.nn as nn
import torch.nn.init as init
import math
from torch.distributions import Categorical
from torch.distributions import MultivariateNormal
import torch.nn.functional as F
from scipy.stats import truncnorm
import numpy as np
import random
from torch.utils.data.sampler import BatchSampler, SubsetRandomSampler



class Normalizer():
    """
    Normalize the state and use it as input.
    """
    def __init__(self, num_inputs):
        self.n = torch.zeros(1, num_inputs, dtype=torch.double)
        self.mean = torch.zeros(1, num_inputs, dtype=torch.double)
        self.mean_diff = torch.zeros(1, num_inputs, dtype=torch.double)
        self.var = torch.zeros(1, num_inputs, dtype=torch.double)

    def observe(self, x):
        self.n += 1.
        last_mean = self.mean.clone()
        self.mean += (x-self.mean)/self.n
        self.mean_diff += (x-last_mean)*(x-self.mean)
        self.var = torch.clamp(self.mean_diff/self.n, min=1e-2)

    def normalize(self, inputs):
        obs_std = torch.sqrt(self.var)
        return (inputs - self.mean)/obs_std


def weight_init(m):
    '''
    Usage:
        model = Model()
        model.apply(weight_init)
    '''
    if isinstance(m, nn.Conv1d):
        init.normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.Conv2d):
        init.xavier_normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.Conv3d):
        init.xavier_normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.ConvTranspose1d):
        init.normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.ConvTranspose2d):
        init.xavier_normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.ConvTranspose3d):
        init.xavier_normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.BatchNorm1d):
        init.normal_(m.weight.data, mean=1, std=0.02)
        init.constant_(m.bias.data, 0)
    elif isinstance(m, nn.BatchNorm2d):
        init.normal_(m.weight.data, mean=1, std=0.02)
        init.constant_(m.bias.data, 0)
    elif isinstance(m, nn.BatchNorm3d):
        init.normal_(m.weight.data, mean=1, std=0.02)
        init.constant_(m.bias.data, 0)
    elif isinstance(m, nn.Linear):
        init.xavier_normal_(m.weight.data)
        init.normal_(m.bias.data)
    elif isinstance(m, nn.LSTM):
        for param in m.parameters():
            if len(param.shape) >= 2:
                init.orthogonal_(param.data)
            else:
                init.normal_(param.data)
    elif isinstance(m, nn.LSTMCell):
        for param in m.parameters():
            if len(param.shape) >= 2:
                init.orthogonal_(param.data)
            else:
                init.normal_(param.data)
    elif isinstance(m, nn.GRU):
        for param in m.parameters():
            if len(param.shape) >= 2:
                init.orthogonal_(param.data)
            else:
                init.normal_(param.data)
    elif isinstance(m, nn.GRUCell):
        for param in m.parameters():
            if len(param.shape) >= 2:
                init.orthogonal_(param.data)
            else:
                init.normal_(param.data)


def triangulate_position(flags, flag_dict):
    """
    By using the info in a see message, estimate the position of the player.
    """

    if len(flags) <= 1:
        return [None, None]
    flags.sort(key=lambda flag: flag.distance)
    points = []
    if len(flags) > 1:
        for i in range(len(flags)-1):
            for j in range(len(flags)-i-1):
                flag_coord1 = np.array(list(flag_dict[flags[i].flag_id]))
                flag_coord2 = np.array(list(flag_dict[flags[j + 1].flag_id]))
                b = euclidean_distance(flag_coord1, flag_coord2)
                a = flags[i].distance
                c = flags[j+1].distance
                if a == 0:
                    points.append(flag_coord1)
                elif b == 0:
                    points.append(flag_coord2)
                else:
                    cos = np.clip(float(c * c - a * a - b * b) / float(-2.0 * a * b), -1, 1)
                    sin = np.sin(np.arccos(cos))
                    vec1 = (flag_coord2 - flag_coord1)/b
                    vec21 = np.array([vec1[1], -vec1[0]])
                    vec22 = np.array([-vec1[1], vec1[0]])
                    point1 = flag_coord1 + cos*vec1*a + sin*vec21*a
                    point2 = flag_coord1 + cos*vec1*a + sin*vec22*a
                    if (point1[0] < 58 and point1[0] > -58 and
                            point1[1] > -40 and point1[1] < 40 ):
                        points.append(point1)
                    if (point2[0] < 58 and point2[0] > -58 and
                            point2[1] > -40 and point2[1] < 40):
                        points.append(point2)

    center_with_most_points = [None, None]
    if len(points) > 1:
        clusters = cluster_points(points)
        max_points = 0
        for c in clusters:
            if len(clusters[c]) > max_points:
                center_with_most_points = c
                max_points = len(clusters[c])

        return [round(center_with_most_points[0], 1), round(center_with_most_points[1], 1)]
    elif len(points) == 1:
        temp = points[0]
        return [round(temp[0], 1),  round(temp[1], 1)]
    else:
        return [None, None]

def absolute_body_angle(position, flags, flag_dict, relative_head_angle=0):
    """
    By using the flag info in a see message and the estimated position, calculate the absolute neck angle
    and therefore the absolute body angle. Currently, we don't have turn_neck action, thus the body angle
    is also the direction that the player is facing. We can introduce head angle later by setting relative_head_angle.
    """
    angles = []
    record = []

    for i in flags:
        flag_coord = np.array(list(flag_dict[i.flag_id]))
        vector_fp = [position[0] - flag_coord[0], position[1] - flag_coord[1]]
        inner_product = -vector_fp[0]
        distance = euclidean_distance(flag_coord, position)
        if distance == 0:
            continue
        cos_theta = inner_product / distance
        theta = math.degrees(np.arccos(cos_theta))
        if flag_coord[1] < position[1]:
            angles.append(-theta - i.direction)
        elif flag_coord[1] > position[1]:
            angles.append(theta - i.direction)
        else:
            angles.append(theta)
        record.append([i.flag_id, i.direction, theta])
    absolute_head_angle = np.mean(angles)
    return absolute_head_angle - relative_head_angle

def ball_position(abs_coords, abs_neck_dir,ball_distance, ball_direction):
    x = abs_coords[0] + math.sin(math.radians(ball_direction+abs_neck_dir))*ball_distance
    y = abs_coords[1] + math.cos(math.radians(ball_direction+abs_neck_dir))*ball_distance
    return x, y

def cluster_points(points, num_cluster_iterations=15):
    """
    Cluster a set of points into a dict of centers mapped to point lists.
    Uses the k-means clustering algorithm with random initial centers and a
    fixed number of iterations to find clusters.
    """

    # generate initial random centers, ignoring identical ones
    centers = set([])
    for i in range(int(math.sqrt(len(points) / 2))):
        # a random coordinate somewhere within the field boundaries
        rand_center = (random.randint(-53, 53), random.randint(-34, 34))
        centers.add(rand_center)
    # cluster for some iterations before the latest result
    latest = {}
    cur = {}
    for i in range(num_cluster_iterations):
        # initialze cluster lists
        for c in centers:
            cur[c] = []

        # put every point into the list of its nearest cluster center
        for p in points:
            # get a list of (distance to center, center coords) tuples
            c_dists = map(lambda c: (euclidean_distance(c, p), c),
                         centers)

            # find the smallest tuple's c (second item)
            nearest_center = min(c_dists)[1]

            # add point to this center's cluster
            cur[nearest_center].append(p)

        # recompute centers
        new_centers = set([])
        for cluster in cur.values():
            tot_x = 0
            tot_y = 0

            # remove empty clusters
            if len(cluster) == 0:
                continue

            # generate average center of cluster
            for p in cluster:
                tot_x += p[0]
                tot_y += p[1]

            # get average center and add to new centers set
            ave_center = (tot_x / len(cluster), tot_y / len(cluster))
            new_centers.add(ave_center)

        # move on to next iteration
        centers = new_centers
        latest = cur
        cur = {}

    # return latest cluster iteration
    return latest

def euclidean_distance(point1, point2):
    """
    Returns the Euclidean distance between two points on a plane.
    """

    x1 = point1[0]
    y1 = point1[1]
    x2 = point2[0]
    y2 = point2[1]

    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

def angle_between_points(point1, point2):
    """
    Returns the angle from the first point to the second, assuming that
    these points exist on a plane, and that the positive x-axis is 0 degrees
    and the positive y-axis is 90 degrees.  All returned angles are positive
    and relative to the positive x-axis.
    """

    x1 = point1[0]
    y1 = point1[1]
    x2 = point2[0]
    y2 = point2[1]

    # get components of vector between the points
    dx = x2 - x1
    dy = y2 - y1

    # return the angle in degrees
    a = math.degrees(math.atan2(dy, dx))

    return a


def init_weights(m):
    if isinstance(m, nn.Linear):
        torch.nn.init.xavier_uniform(m.weight)
        if m.bias is not None:
            m.bias.data.fill_(0.01)

def init_weights2(module):
    if isinstance(module, nn.Linear):
        module.weight.data.normal_(mean=0.0, std=0.1)
        if module.bias is not None:
            module.bias.data.zero_()

