import math
import random
import numpy as np
import sys
import utils
sys.path.append("..")
import game_object


class WorldModel:
    """
    Holds and updates the model of the world as known from current and past
    data.
    """
    # constants for team sides
    SIDE_L = "l"
    SIDE_R = "r"

    class PlayModes:
        """
        Acts as a static class containing variables for all valid play modes.
        The string values correspond to what the referee calls the game modes.
        """

        BEFORE_KICK_OFF = "before_kick_off"
        PLAY_ON = "play_on"
        TIME_OVER = "time_over"
        KICK_OFF_L = "kick_off_l"
        KICK_OFF_R = "kick_off_r"
        KICK_IN_L = "kick_in_l"
        KICK_IN_R = "kick_in_r"
        FREE_KICK_L = "free_kick_l"
        FREE_KICK_R = "free_kick_r"
        CORNER_KICK_L = "corner_kick_l"
        CORNER_KICK_R = "corner_kick_r"
        GOAL_KICK_L = "goal_kick_l"
        GOAL_KICK_R = "goal_kick_r"
        DROP_BALL = "drop_ball"
        OFFSIDE_L = "offside_l"
        OFFSIDE_R = "offside_r"

        def __init__(self):
            raise NotImplementedError("Don't instantiate a PlayModes class,"
                    " access it statically through WorldModel instead.")

    class RefereeMessages:
        """
        Static class containing possible non-mode messages sent by a referee.
        """

        # these are referee messages, not play modes
        FOUL_L = "foul_l"
        FOUL_R = "foul_r"
        GOALIE_CATCH_BALL_L = "goalie_catch_ball_l"
        GOALIE_CATCH_BALL_R = "goalie_catch_ball_r"
        TIME_UP_WITHOUT_A_TEAM = "time_up_without_a_team"
        TIME_UP = "time_up"
        HALF_TIME = "half_time"
        TIME_EXTENDED = "time_extended"

        # these are special, as they are always followed by '_' and an int of
        # the number of goals scored by that side so far.  these won't match
        # anything specifically, but goals WILL start with these.
        GOAL_L = "goal_l_"
        GOAL_R = "goal_r_"

        def __init__(self):
            raise NotImplementedError("Don't instantiate a RefereeMessages class,"
                    " access it statically through WorldModel instead.")

    def __init__(self, action_handler, port):
        """
        Create the world model with default values and an ActionHandler class it
        can use to complete requested actions.
        """

        # we use the action handler to complete complex commands
        self.ah = action_handler

        # these variables store all objects for any particular game step
        self.ball = None
        self.flags = []

        self.players = []
        self.lines = []
        self.goal_l = [-52.5, 0]
        self.goal_r = [52.5, 0]

        # scores for each side
        self.score_l = 0
        self.score_r = 0

        # handle player information, like uniform number and side
        self.side = None
        self.uniform_number = None

        # stores the most recent message heard
        self.last_message = None

        # the mode the game is currently in (default to not playing yet)
        self.play_mode = WorldModel.PlayModes.BEFORE_KICK_OFF

        # body state
        self.view_width = None
        self.view_quality = None
        self.stamina = None
        self.effort = None
        self.capacity = None
        self.speed_amount = 0
        self.speed_direction = 0
        self.neck_direction = None

        # counts of actions taken so far
        self.action_counts = {}
        self.action_counts["kick_count"] = 0
        self.action_counts["dash_count"]= 0
        self.action_counts["turn_count"] = 0
        self.action_counts["say_count"] = 0
        self.action_counts["turn_neck_count"] = 0
        self.action_counts["catch_count"] = 0
        self.action_counts["move_count"] = 0
        self.action_counts["change_view_count"] = 0

        # apparent absolute player coordinates and neck/body directions
        self.abs_coords = None
        self.abs_neck_angle = None
        self.abs_body_angle = None

        # create a new server parameter object for holding all server params
        self.server_parameters = ServerParameters()

        # counts the number of cycles
        self.see_sim_time = 0
        self.sense_body_sim_time = 0
        self.hear_sim_time = 0

        '''
        For coach 
        '''
        self.ball_coords = None
        self.agent_coords = None
        self.agent_ball_distance = None
        self.ball_goal_distance = None
        self.sim_time = 0
        self.episode_start_time = 0
        self.state = None
        self.done = False
        self.reward = 0
        self.episode_start = False
        self.dash_reward = 0
        self.coach_kick_count = 0

        self.last_state = None
        # In the case where the player can't collect enough visual information to determine where it is, we would
        # use this info to estimate. May also need last action. E.g. Turn/dash

        self.teamname = None
        self.port = int(port)
        self.if_kick_off = True



    def process_new_info(self, ball, flags, goals, players, lines):
        """
        Update any internal variables based on the currently available
        information.  This also calculates information not available directly
        from server-reported messages, such as player coordinates.
        Generate state.
        """

        # update basic information
        self.ball = ball
        self.flags = flags
        self.goals = goals
        self.players = players
        self.lines = lines

        agent_state = [self.speed_amount / 1.05 + 1, self.speed_direction / 180.0 + 2]
        flag_state = [0] * 110
        for i in self.flags:

            n = game_object.Flag.FLAG_NUMBERS.index(i.flag_id)
            if i.distance is None:
                continue
            flag_state[2 * n] = i.direction / 180.0 + 2
            flag_state[2 * n + 1] = i.distance / 100.0 + 1

        # Rescale the distance and direction of flags approximately within (1, 2).
        # Absent flag would be represented by (0, 0).
        if self.ball is None:
            return np.array(agent_state + [0, 0, 0, 0] + flag_state)
        ball_state = [self.ball.distance, self.ball.direction, self.ball.dist_change, self.ball.dir_change]
        for i,j in enumerate(ball_state):
            if j is None:
                ball_state[i] = 0
            else:
                if i == 0:
                    ball_state[i] = j/100.0 + 1
                elif i == 1:
                    ball_state[i] = j/180.0 + 2
                elif i == 2:
                    ball_state[i] = j/3.0 + 1
                    # ball max speed: 3
                else:
                    ball_state[i] = j/180.0 + 2



        """
        agent_state = [self.stamina, self.effort, self.capacity]
        palyer_state = []
        """

        return np.array(agent_state + ball_state + flag_state)



    def is_before_kick_off(self):
        """
        Tells us whether the game is in a pre-kickoff state.
        """

        return self.play_mode == WorldModel.PlayModes.BEFORE_KICK_OFF

    def is_kick_off_us(self):
        """
        Tells us whether it's our turn to kick off.
        """

        ko_left = WorldModel.PlayModes.KICK_OFF_L
        ko_right = WorldModel.PlayModes.KICK_OFF_R

        # return whether we're on the side that's kicking off
        return (self.side == WorldModel.SIDE_L and self.play_mode == ko_left or
                self.side == WorldModel.SIDE_R and self.play_mode == ko_right)

    def is_dead_ball_them(self):
        """
        Returns whether the ball is in the other team's posession and it's a
        free kick, corner kick, or kick in.
        """

        # shorthand for verbose constants
        kil = WorldModel.PlayModes.KICK_IN_L
        kir = WorldModel.PlayModes.KICK_IN_R
        fkl = WorldModel.PlayModes.FREE_KICK_L
        fkr = WorldModel.PlayModes.FREE_KICK_R
        ckl = WorldModel.PlayModes.CORNER_KICK_L
        ckr = WorldModel.PlayModes.CORNER_KICK_R

        # shorthand for whether left team or right team is free to act
        pm = self.play_mode
        free_left = (pm == kil or pm == fkl or pm == ckl)
        free_right = (pm == kir or pm == fkr or pm == ckr)

        # return whether the opposing side is in a dead ball situation
        if self.side == WorldModel.SIDE_L:
            return free_right
        else:
            return free_left

    def is_ball_kickable(self):
        """
        Tells us whether the ball is in reach of the current player.
        """

        # ball must be visible, not behind us, and within the kickable margin
        return (self.ball is not None and
                self.ball.distance is not None and
                self.ball.distance <= self.server_parameters.kickable_margin)

    def get_ball_speed_max(self):
        """
        Returns the maximum speed the ball can be kicked at.
        """

        return self.server_parameters.ball_speed_max

    def kick_to(self, point, extra_power=0.0):
        """
        Kick the ball to some point with some extra-power factor added on.
        extra_power=0.0 means the ball should stop at the given point, anything
        higher means it should have proportionately more speed.
        """

        # how far are we from the desired point?
        point_dist = utils.euclidean_distance(self.abs_coords, point)

        # get absolute direction to the point
        abs_point_dir = utils.angle_between_points(self.abs_coords, point)

        # get relative direction to point from body, since kicks are relative to
        # body direction.
        if self.abs_body_angle is not None:
            rel_point_angle = self.abs_body_angle - abs_point_dir

        # we do a simple linear interpolation to calculate final kick speed,
        # assuming a kick of power 100 goes 45 units in the given direction.
        # these numbers were obtained from section 4.5.3 of the documentation.
        # TODO: this will fail if parameters change, needs to be more flexible
        max_kick_dist = 45.0
        dist_ratio = point_dist / max_kick_dist

        # find the required power given ideal conditions, then add scale up by
        # difference bewteen actual aceivable power and maxpower.
        required_power = dist_ratio * self.server_parameters.maxpower
        effective_power = self.get_effective_kick_power(self.ball,
                required_power)
        required_power += 1 - (effective_power / required_power)

        # add more power!
        power_mod = 1.0 + extra_power
        power = required_power * power_mod

        # do the kick, finally
        self.ah.kick(rel_point_dir, power)

    def get_effective_kick_power(self, ball, power):
        """
        Returns the effective power of a kick given a ball object.  See formula
        4.21 in the documentation for more details.
        """

        # we can't calculate if we don't have a distance to the ball
        if ball.distance is None:
            return

        # first we get effective kick power:
        # limit kick_power to be between minpower and maxpower
        kick_power = max(min(power, self.server_parameters.maxpower),
                self.server_parameters.minpower)

        # scale it by the kick_power rate
        kick_power *= self.server_parameters.kick_power_rate

        # now we calculate the real effective power...
        a = 0.25 * (ball.direction / 180)
        b = 0.25 * (ball.distance / self.server_parameters.kickable_margin)

        # ...and then return it
        return 1 - a - b

    def turn_neck_to_object(self, obj):
        """
        Turns the player's neck to a given object.
        """

        self.ah.turn_neck(obj.direction)

    def get_distance_to_point(self, point):
        """
        Returns the linear distance to some point on the field from the current
        point.
        """

        return utils.euclidean_distance(self.abs_coords, point)

    def turn_body_to_point(self, point):
        """
        Turns the agent's body to face a given point on the field.
        """

        # calculate absolute direction to point
        abs_point_dir = utils.angle_between_points(self.abs_coords, point)

        # subtract from absolute body direction to get relative angle
        relative_dir = self.abs_body_angle - abs_point_dir

        # turn to that angle
        self.ah.turn(relative_dir)

    def get_object_absolute_coords(self, obj):
        """
        Determines the absolute coordinates of the given object based on the
        agent's current position.  Returns None if the coordinates can't be
        calculated.
        """

        # we can't calculate this without a distance to the object
        if obj.distance is None or self.abs_coords[0] is None:
            return None

        # get the components of the vector to the object
        dx = obj.distance * math.cos(obj.direction)
        dy = obj.distance * math.sin(obj.direction)

        # return the point the object is at relative to our current position
        return (self.abs_coords[0] + dx, self.abs_coords[1] + dy)

    def teleport_to_point(self, point):
        """
        Teleports the player to a given (x, y) point using the 'move' command.
        """

        self.ah.move(point[0], point[1])

    def align_neck_with_body(self):
        """
        Turns the player's neck to be in line with its body, making the angle
        between the two 0 degrees.
        """

        # neck angle is relative to body, so we turn it back the inverse way
        if self.neck_direction is not None:
            self.ah.turn_neck(self.neck_direction * -1)

    def get_nearest_teammate_to_point(self, point):
        """
        Returns the uniform number of the fastest teammate to some point.
        """

        # holds tuples of (player dist to point, player)
        distances = []
        for p in self.players:
            # skip enemy and unknwon players
            if p.teamname != self.teamname:
                continue

            # find their absolute position
            p_coords = self.get_object_absolute_coords(p)

            distances.append((utils.euclidean_distance(point, p_coords), p))

        # return the nearest known teammate to the given point
        nearest = min(distances)[1]
        return nearest



    '''
    Unimplemented methods 
    
        def get_stamina(self):
        """
        Returns the agent's current stamina amount.
        """

        return self.stamina

    def get_stamina_max(self):
        """
        Returns the maximum amount of stamina a player can have.
        """

        return self.server_parameters.stamina_max

    def turn_body_to_object(self, obj):
        """
        Turns the player's body to face a particular object.
        """

        self.ah.turn(obj.direction)
    
    '''


class ServerParameters:
    """
    A storage container for all the settings of the soccer server.
    """

    def __init__(self):
        """
        Initialize default parameters for a server.
        """

        self.audio_cut_dist=50
        self.auto_mode = 0
        self.back_dash_rate = 0.7
        self.back_passes = 1
        self.ball_accel_max = 2.7
        self.ball_decay = 0.94
        self.ball_rand = 0.05
        self.ball_size = 0.085
        self.ball_speed_max = 3
        self.ball_stuck_area = 3
        self.ball_weight = 0.2
        self.catch_ban_cycle = 5
        self.catch_probability = 1
        self.catchable_area_l = 1.2
        self.catchable_area_w = 1
        self.ckick_margin = 1
        self.clang_advice_win = 1
        self.clang_define_win = 1
        self.clang_del_win = 1
        self.clang_info_win = 1
        self.clang_mess_delay = 50
        self.clang_mess_per_cycle = 1
        self.clang_meta_win = 1
        self.clang_rule_win = 1
        self.clang_win_size = 300
        self.coach = 1
        self.coach_port = 6001
        self.coach_w_referee = 0
        self.connect_wait = 300
        self.control_radius = 2
        self.dash_angle_step = 1
        self.dash_power_rate = 0.006
        self.drop_ball_time = 100
        self.effort_dec = 0.005
        self.effort_dec_thr = 0.3
        self.effort_inc = 0.01
        self.effort_inc_thr = 0.6
        self.effort_init = 1
        self.effort_min = 0.6
        self.extra_half_time = 100
        self.extra_stamina = 50
        self.fixed_teamname_l = ""
        self.fixed_teamname_r = ""
        self.forbid_kick_off_offside = 1
        self.foul_cycles = 5
        self.foul_detect_probability = 0.5
        self.foul_exponent = 10
        self.free_kick_faults = 1
        self.freeform_send_period = 20
        self.freeform_wait_period = 600
        self.fullstate_l = 0
        self.fullstate_r = 0
        self.game_log_compression = 0
        self.game_log_dated = 1
        self.game_log_dir = "./"
        self.game_log_fixed = 0
        self.game_log_fixed_name = "rcssserver"
        self.game_log_version = 6
        self.game_logging = 1
        self.game_over_wait = 100
        self.goal_width = 14.02
        self.goalie_max_moves = 2
        self.golden_goal = 0
        self.half_time = 300
        self.hear_decay = 1
        self.hear_inc = 1
        self.hear_max = 1
        self.illegal_defense_dist_x = 16.5
        self.illegal_defense_duration = 20
        self.illegal_defense_number = 0
        self.illegal_defense_width = 40.32
        self.inertia_moment = 5
        self.keepaway = 0
        self.keepaway_length = 20
        self.keepaway_log_dated = 1
        self.keepaway_log_dir = "./"
        self.keepaway_log_fixed = 0
        self.keepaway_log_fixed_name = "rcssserver"
        self.keepaway_logging = 1
        self.keepaway_start = -1
        self.keepaway_width = 20
        self.kick_off_wait = 100
        self.kick_power_rate = 0.027
        self.kick_rand = 0.1
        self.kick_rand_factor_l = 1
        self.kick_rand_factor_r = 1
        self.kickable_margin = 0.7
        self.landmark_file = "~/.rcssserver-landmark.xml"
        self.log_date_format = "%Y%m%d%H%M%S-"
        self.log_times = 0
        self.max_back_tackle_power = 0
        self.max_catch_angle = 90
        self.max_dash_angle = 180
        self.max_dash_power = 100
        self.max_goal_kicks = 3
        self.max_tackle_power = 100
        self.maxmoment = 180
        self.maxneckang = 90
        self.maxneckmoment = 180
        self.maxpower = 100
        self.min_catch_angle = -90
        self.min_dash_angle = -180
        self.min_dash_power = 0
        self.minmoment = -180
        self.minneckang = -90
        self.minneckmoment = -180
        self.minpower = -100
        self.nr_extra_halfs = 2
        self.nr_normal_halfs = 2
        self.offside_active_area_size = 2.5
        self.offside_kick_margin = 9.15
        self.olcoach_port = 6002
        self.old_coach_hear = 0
        self.pen_allow_mult_kicks = 1
        self.pen_before_setup_wait = 10
        self.pen_coach_moves_players = 1
        self.pen_dist_x = 42.5
        self.pen_max_extra_kicks = 5
        self.pen_max_goalie_dist_x = 14
        self.pen_nr_kicks = 5
        self.pen_random_winner = 0
        self.pen_ready_wait = 10
        self.pen_setup_wait = 70
        self.pen_taken_wait = 150
        self.penalty_shoot_outs = 1
        self.player_accel_max = 1
        self.player_decay = 0.4
        self.player_rand = 0.1
        self.player_size = 0.3
        self.player_speed_max = 1.05
        self.player_speed_max_min = 0.75
        self.player_weight = 60
        self.point_to_ban = 5
        self.point_to_duration = 20
        self.port = 6000
        self.prand_factor_l = 1
        self.prand_factor_r = 1
        self.profile = 0
        self.proper_goal_kicks = 0
        self.quantize_step = 0.1
        self.quantize_step_l = 0.01
        self.record_messages = 0
        self.recover_dec = 0.002
        self.recover_dec_thr = 0.3
        self.recover_init = 1
        self.recover_min = 0.5
        self.recv_step = 10
        self.red_card_probability = 0
        self.say_coach_cnt_max = 128
        self.say_coach_msg_size = 128
        self.say_msg_size = 10
        self.send_comms = 0
        self.send_step = 150
        self.send_vi_step = 100
        self.sense_body_step = 100
        self.side_dash_rate = 0.4
        self.simulator_step = 100
        self.slow_down_factor = 1
        self.slowness_on_top_for_left_team = 1
        self.slowness_on_top_for_right_team = 1
        self.stamina_capacity = 130600
        self.stamina_inc_max = 45
        self.stamina_max = 8000
        self.start_goal_l = 0
        self.start_goal_r = 0
        self.stopped_ball_vel = 0.01
        self.synch_micro_sleep = 1
        self.synch_mode = 0
        self.synch_offset = 60
        self.synch_see_offset = 0
        self.tackle_back_dist = 0
        self.tackle_cycles = 10
        self.tackle_dist = 2
        self.tackle_exponent = 6
        self.tackle_power_rate = 0.027
        self.tackle_rand_factor = 2
        self.tackle_width = 1.25
        self.team_actuator_noise = 0
        self.team_l_start = ""
        self.team_r_start = ""
        self.text_log_compression = 0
        self.text_log_dated = 1
        self.text_log_dir = "./"
        self.text_log_fixed = 0
        self.text_log_fixed_name = "rcssserver"
        self.text_logging = 1
        self.use_offside = 1
        self.verbose = 0
        self.visible_angle = 90
        self.visible_distance = 3
        self.wind_ang = 0
        self.wind_dir = 0
        self.wind_force = 0
        self.wind_none = 0
        self.wind_rand = 0
        self.wind_random = 0









