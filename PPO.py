# https://github.com/nikhilbarhate99/PPO-PyTorch/blob/master/PPO.py
import gymnasium as gym

import utils
from RcssserverEnv import RcssserverEnv
import numpy as np
from torch.utils.tensorboard import SummaryWriter
import torch
import torch.nn as nn
from torch.distributions import MultivariateNormal
from torch.distributions import Categorical

################################## set device ##################################
print("============================================================================================")
# set device to cpu or cuda
device = torch.device('cpu')
if (torch.cuda.is_available()):
    device = torch.device('cuda:0')
    torch.cuda.empty_cache()
    print("Device set to : " + str(torch.cuda.get_device_name(device)))
else:
    print("Device set to : cpu")
print("============================================================================================")


################################## PPO Policy ##################################
class RolloutBuffer:
    def __init__(self):
        self.actions = []
        self.parameters = []
        self.states = []
        self.action_logprobs = []
        self.param_logprobs = []
        self.rewards = []
        self.state_values = []
        self.is_terminals = []

    def clear(self):
        del self.actions[:]
        del self.states[:]
        del self.parameters[:]
        del self.action_logprobs[:]
        del self.param_logprobs[:]
        del self.rewards[:]
        del self.state_values[:]
        del self.is_terminals[:]

class TwoHeadedNetwork(nn.Module):
    def __init__(self, input_dim, parameter_dims):
        super(TwoHeadedNetwork, self).__init__()
        self.head1 = nn.Sequential(
            nn.Linear(input_dim, 256, bias=False),
            nn.Tanh(),
            nn.Linear(256, 128, bias=False),
            nn.Tanh(),
            nn.Linear(128, parameter_dims[0], bias=False),
            nn.Tanh()
        )
        self.head2 = nn.Sequential(
            nn.Linear(input_dim, 256, bias=False),
            nn.Tanh(),
            nn.Linear(256, 128, bias=False),
            nn.Tanh(),
            nn.Linear(128, parameter_dims[1], bias=False),
            nn.Tanh()
        )


    def forward(self, x):
        out1 = self.head1(x)
        out2 = self.head2(x)

        if out1.dim() == 1:
            p = torch.cat((out1, out2))
        else:
            p = torch.cat((out1, out2), 1)
        return p

class ActorCritic(nn.Module):
    def __init__(self, state_dim, action_dim, parameter_dim, action_std_init):
        super(ActorCritic, self).__init__()


        self.parameter_dim = parameter_dim
        self.action_dim = action_dim
        self.parameter_var = torch.full((sum(parameter_dim),), action_std_init * action_std_init).to(device)
        '''
        self.parameter_var = {}
        for i, j in enumerate(self.parameter_dim):
            self.parameter_var[i] = torch.full((j,), action_std_init * action_std_init).to(device)
        # actor
      
        self.actor = nn.Sequential(
            nn.Linear(state_dim, 256, bias=False),
            nn.ReLU(),
            nn.Linear(256, 128, bias=False),
            nn.ReLU(),
            nn.Linear(128, 128, bias=False),
            nn.ReLU(),
            nn.Linear(128, 128, bias=False),
            nn.ReLU()
        )
        '''
        self.action_head = nn.Sequential(
            nn.Linear(state_dim, 256),
            nn.ReLU(),
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, action_dim, bias=False),
            nn.Softmax()
        )

        # 第二个动作类型的输出层
        # self.parameter_head = TwoHeadedNetwork(state_dim, parameter_dim)
        self.parameter_head = nn.Sequential(
            nn.Linear(state_dim, 256),
            nn.ReLU(),
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, sum(parameter_dim), bias=False),
            nn.Tanh()
        )

        # critic
        self.critic = nn.Sequential(
            nn.Linear(state_dim, 256, bias=False),
            nn.ReLU(),
            nn.Linear(256, 256, bias=False),
            nn.ReLU(),
            nn.Linear(256, 128, bias=False),
            nn.ReLU(),
            nn.Linear(128, 1, bias=False)
        )

    def set_action_std(self, new_action_std):


        self.parameter_var = torch.full((sum(self.parameter_dim),), new_action_std * new_action_std).to(device)

    def forward(self):
        raise NotImplementedError

    def act(self, state):
        # action_probs = self.action_head(self.actor(state))
        action_probs = self.action_head(state)
        dist1 = Categorical(action_probs)
        action = dist1.sample()
        action_logprob = dist1.log_prob(action)

        start = sum(self.parameter_dim[:action])
        # parameter_mean = self.parameter_head(self.actor(state))[start:start+self.parameter_dim[action]]
        parameter_mean = self.parameter_head(state)
        cov_mat = torch.diag(self.parameter_var).unsqueeze(dim=0)
        parameter_mean = parameter_mean.float()
        cov_mat = cov_mat.float()
        dist2 = MultivariateNormal(parameter_mean, cov_mat)
        parameter_all = dist2.sample()
        parameter_logprob = dist2.log_prob(parameter_all)
        state_val = self.critic(state)
        parameter = parameter_all.squeeze(0)[start:start+self.parameter_dim[action]]

        return action.detach(), parameter.detach(), action_logprob.detach(), parameter_logprob, state_val.detach(), parameter_all.detach()

    def evaluate(self, state, action, parameter_all):
        # action_probs = self.action_head(self.actor(state))
        action_probs = self.action_head(state)
        dist1 = Categorical(action_probs)
        action_logprob = dist1.log_prob(action)
        action_entropy = dist1.entropy()

        # parameter_mean = self.parameter_head(self.actor(state))
        parameter_mean = self.parameter_head(state)

        cov_mat = torch.diag(self.parameter_var).unsqueeze(dim=0)
        parameter_mean = parameter_mean.float()
        cov_mat = cov_mat.float()

        dist2 = MultivariateNormal(parameter_mean, cov_mat)
        param_entropy = dist2.entropy()

        parameter_logprob = dist2.log_prob(parameter_all)

        return action_logprob, parameter_logprob, self.critic(state), action_entropy, param_entropy

class PPO:
    def __init__(self, state_dim, action_dim, parameter_dim, max_action, gamma=0.99, eps_clip=0.3, K_epochs=50,
            action_std_init=0.1, lr_actor=5e-4, lr_critic=1e-3):

        self.parameter_dim = parameter_dim
        self.action_std = action_std_init
        self.gamma = gamma
        self.eps_clip = eps_clip
        self.K_epochs = K_epochs
        self.max_action = max_action

        self.buffer = RolloutBuffer()

        self.policy = ActorCritic(state_dim, action_dim, parameter_dim, action_std_init).to(device)
        # Initialization of the nn
        '''
        self.policy.parameter_head.apply(utils.init_weights)
        self.policy.action_head.apply(utils.init_weights)
        self.policy.actor.apply(utils.init_weights)
        self.policy.critic.apply(utils.init_weights)
        '''
        self.optimizer = torch.optim.Adam([
            # {'params': self.policy.actor.parameters(), 'lr': lr_actor},
            {'params': self.policy.action_head.parameters(), 'lr': lr_actor},
            {'params': self.policy.parameter_head.parameters(), 'lr': lr_actor},
            {'params': self.policy.critic.parameters(), 'lr': lr_critic}
        ])

        self.policy_old = ActorCritic(state_dim, action_dim, parameter_dim, action_std_init).to(device)
        self.policy_old.load_state_dict(self.policy.state_dict())

        self.MseLoss = nn.MSELoss()
        self.writer = SummaryWriter(log_dir='runs/PPO')

    def set_action_std(self, new_action_std):
            self.action_std = new_action_std
            self.policy.set_action_std(new_action_std)
            self.policy_old.set_action_std(new_action_std)

    def decay_action_std(self, action_std_decay_rate, min_action_std):
        print("--------------------------------------------------------------------------------------------")

        self.action_std = self.action_std - action_std_decay_rate
        if (self.action_std <= min_action_std):
            self.action_std = min_action_std
            print("setting actor output action_std to min_action_std : ", self.action_std)
        else:
            print("setting actor output action_std to : ", self.action_std)
        self.set_action_std(self.action_std)
        print("--------------------------------------------------------------------------------------------")

    def select_action(self, state):

        with torch.no_grad():
            state = torch.FloatTensor(state).to(device)
            action, parameter, action_logprob, param_logprob, state_val, parameter_all = self.policy_old.act(state)

        self.buffer.states.append(state)
        self.buffer.actions.append(action)
        self.buffer.parameters.append(parameter_all)
        self.buffer.action_logprobs.append(action_logprob)
        self.buffer.param_logprobs.append(param_logprob)
        self.buffer.state_values.append(state_val)

        return action.detach().cpu().numpy().flatten(), parameter.detach().cpu().numpy().flatten()


    def update(self):
       
        # Monte Carlo estimate of returns
        rewards = []
        discounted_reward = 0
        for reward, is_terminal in zip(reversed(self.buffer.rewards), reversed(self.buffer.is_terminals)):
            if is_terminal:
                discounted_reward = 0
            discounted_reward = reward + (self.gamma * discounted_reward)
            rewards.insert(0, discounted_reward)

        # Normalizing the rewards
        rewards = torch.tensor(rewards, dtype=torch.float32).to(device)
        rewards = (rewards - rewards.mean()) / (rewards.std() + 1e-7)

        # convert list to tensor
        old_states = torch.squeeze(torch.stack(self.buffer.states, dim=0)).detach().to(device)
        old_actions = torch.squeeze(torch.stack(self.buffer.actions, dim=0)).detach().to(device)
        old_parameters = torch.squeeze(torch.stack(self.buffer.parameters, dim=0)).detach().to(device)
        old_action_logprobs = torch.squeeze(torch.stack(self.buffer.action_logprobs, dim=0)).detach().to(device)
        old_param_logprobs = torch.squeeze(torch.stack(self.buffer.param_logprobs, dim=0)).detach().to(device)
        old_state_values = torch.squeeze(torch.stack(self.buffer.state_values, dim=0)).detach().to(device)

        # calculate advantages
        advantages = rewards.detach() - old_state_values.detach()

        # Optimize policy for K epochs
        for i in range(self.K_epochs):
            # Evaluating old actions and values
            action_logprobs, param_logprobs, state_values, action_entropy, param_entropy = self.policy.evaluate(old_states, old_actions, old_parameters)
            # match state_values tensor dimensions with rewards tensor
            state_values = torch.squeeze(state_values)

            # Finding the ratio (pi_theta / pi_theta__old)
            action_ratios = torch.exp(action_logprobs - old_action_logprobs.detach())
            action_kl = (old_action_logprobs - action_logprobs).mean().item()
            param_ratios = torch.exp(param_logprobs - old_param_logprobs.detach())
            param_kl = (old_param_logprobs - param_logprobs).mean().item()
            # Finding Surrogate Loss
            action_surr1 = action_ratios * advantages

            action_surr2 = torch.clamp(action_ratios, 1 - self.eps_clip, 1 + self.eps_clip) * advantages

            param_surr1 = param_ratios * advantages

            param_surr2 = torch.clamp(param_ratios, 1 - self.eps_clip, 1 + self.eps_clip) * advantages

            l1norm = (# sum(p.abs().sum() for p in self.policy.actor.parameters()) +
                      sum(p.abs().sum() for p in self.policy.critic.parameters()) +
                      sum(p.abs().sum() for p in self.policy.action_head.parameters()) +
                      sum(p.abs().sum() for p in self.policy.parameter_head.parameters()))

            # final loss of clipped objective PPO
            loss = -torch.min(action_surr1, action_surr2) - torch.min(param_surr1, param_surr2) + 0.5 * self.MseLoss(state_values, rewards) - 0.01 * action_entropy - 0.01 * param_entropy + 0.001 * l1norm
            '''
            if action_kl > 0.015:
                self.policy.action_head.requires_grad = False
                print('kl too big for action at epoch {}'.format(i))
            if param_kl > 0.015:
                self.policy.parameter_head.requires_grad = False
                print('kl too big for param at epoch {}'.format(i))
            '''
            # take gradient step
            self.optimizer.zero_grad()
            loss.mean().backward()
            # torch.nn.utils.clip_grad_norm_(self.policy.actor.parameters(), 1)
            torch.nn.utils.clip_grad_norm_(self.policy.action_head.parameters(), 1)
            torch.nn.utils.clip_grad_norm_(self.policy.parameter_head.parameters(), 1)
            torch.nn.utils.clip_grad_norm_(self.policy.critic.parameters(), 1)
            self.optimizer.step()
            '''
            self.policy.action_head.requires_grad = True
            self.policy.parameter_head.requires_grad = True
            '''
        # Copy new weights into old policy
        self.policy_old.load_state_dict(self.policy.state_dict())

        # clear buffer
        self.buffer.clear()

        #self.decay_action_std(0.0001, 0.0001)

    def save(self, checkpoint_path):
        torch.save(self.policy_old.state_dict(), checkpoint_path)

    def load(self, checkpoint_path):
        self.policy_old.load_state_dict(torch.load(checkpoint_path, map_location=lambda storage, loc: storage))
        self.policy.load_state_dict(torch.load(checkpoint_path, map_location=lambda storage, loc: storage))


if __name__ == '__main__':

    gym.register(
        id='rcssserver-env-18.1.3',
        entry_point='rcssserver-env:RcssserverEnv',
    )

    env = RcssserverEnv()

    number = 2
    # Set random seed
    seed = 0

    torch.manual_seed(seed)

    state_dim = env.observation_space.shape[0]
    action_dim = 2
    parameter_dim = [2, 1]
    max_action = np.array([100.0, 90.0, 100.0]) # Maximum number of steps per episode
    print("env={}".format("RcssserverEnv"))
    print("state_dim={}".format(state_dim))
    print("action_dim={}".format(action_dim))
    print("max_action={}".format(max_action))

    agent = PPO(state_dim, action_dim, parameter_dim, max_action)


    max_train_steps = 500000000  # Maximum number of training steps
    update_freq = 50  # Take 50 steps,then update the networks 50 times
    evaluate_rewards = 0  # Record the rewards during the evaluating
    total_steps = 0  # Record the total steps during the training
    total_episodes = 0
    t = 0  # update number
    steps_of_this_update = 0
    temp = []
    last_kick = None
    while total_steps < max_train_steps:
        init = False
        if total_steps == 0:
            init = True

        s, _ = env.reset(init=init)
        done = False


        while not done:

            a, p = agent.select_action(s)
            p = p * max_action[sum(parameter_dim[:int(a[0])]):sum(parameter_dim[:a[0]]) + parameter_dim[a[0]]]


            a = list(a) + list(p)

            s, r, done, _, _ = env.step(a)
            evaluate_rewards += r
            if a[0] == 0 and env.effective_kick:

                if last_kick is None:
                    last_kick = steps_of_this_update
                else:

                    agent.buffer.rewards[last_kick] = env.reward
                    evaluate_rewards += env.reward
                    last_kick = steps_of_this_update
                env.effective_kick = False

            if done and last_kick:

                if last_kick != steps_of_this_update:
                    agent.buffer.rewards[last_kick] = env.reward
                    evaluate_rewards += env.reward
                last_kick = None
            agent.buffer.rewards.append(r)
            agent.buffer.is_terminals.append(done)
            total_steps += 1
            steps_of_this_update += 1

        total_episodes += 1
        # Take 50 steps,then update the networks 50 times
        if total_episodes % update_freq == 0:

            print('The average reward from last {} episodes is {}.'.format(update_freq, evaluate_rewards/update_freq))
            print('The current total_episodes is {}.'.format(total_episodes))
            print('The current total_steps is {}.'.format(total_steps))

            agent.writer.add_scalar('Reward', evaluate_rewards/update_freq, t)
            evaluate_rewards = 0
            agent.update()
            steps_of_this_update = 0
            print('{} update completed.'.format(t + 1))
            '''
            for name, param in agent.policy.actor.named_parameters():

                agent.writer.add_histogram('actor'+name, param.clone().cpu().data.numpy(), t)
            '''
            for name, param in agent.policy.action_head.named_parameters():
                agent.writer.add_histogram('action_head'+name, param.clone().cpu().data.numpy(), t)

            for name, param in agent.policy.parameter_head.named_parameters():
                agent.writer.add_histogram('parameter_head'+name, param.clone().cpu().data.numpy(), t)

            t += 1

            # torch.save(agent.policy.actor.state_dict(), 'runs/PPO/actor')
            torch.save(agent.policy.critic.state_dict(), 'runs/PPO/critic')

    env.close()